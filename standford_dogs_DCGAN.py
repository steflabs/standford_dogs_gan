
# coding: utf-8

# ## DOG DCGAN
# DCGAN para geração de imagens de cachorros, este notebook utiliza do dataset disponivel em https://www.kaggle.com/jessicali9530/stanford-dogs-dataset .

# In[1]:


import pandas as pd
import numpy as np
import tensorflow as tf
import keras
import matplotlib.gridspec as gridspec
import cv2
import os
from PIL import Image
from glob import glob
import matplotlib.pyplot as plt
import math
import time


# #### Preparação do dataset

# In[2]:


dataset_dir = './sep_images/'
resized_dataset_dir = './resized/'

for img in os.listdir(dataset_dir):
    image = cv2.imread(os.path.join(dataset_dir, img))
    image = cv2.resize(image, (128, 128))
    cv2.imwrite(os.path.join(resized_dataset_dir, img), image)


# In[3]:


# This part was taken from Udacity Face generator project
def get_image(image_path, width, height, mode):
    """
    Read image from image_path
    :param image_path: Path of image
    :param width: Width of image
    :param height: Height of image
    :param mode: Mode of image
    :return: Image data
    """
    image = Image.open(image_path)

    return np.array(image.convert(mode))

def get_batch(image_files, width, height, mode):
    data_batch = np.array(
        [get_image(sample_file, width, height, mode) for sample_file in image_files]).astype(np.float32)

    # Make sure the images are in 4 dimensions
    if len(data_batch.shape) < 4:
        data_batch = data_batch.reshape(data_batch.shape + (1,))

    return data_batch

# Essa função foi retirada do helper.py do repo: https://github.com/simoninithomas/CatDCGAN/
def images_square_grid(images, mode):
    """
    Save images as a square grid
    :param images: Images to be used for the grid
    :param mode: The mode to use for images
    :return: Image of images in a square grid
    """
    # Get maximum size for square grid of images
    save_size = math.floor(np.sqrt(images.shape[0]))

    # Scale to 0-255
    images = (((images - images.min()) * 255) / (images.max() - images.min())).astype(np.uint8)

    # Put images in a square arrangement
    images_in_square = np.reshape(
            images[:save_size*save_size],
            (save_size, save_size, images.shape[1], images.shape[2], images.shape[3]))
    if mode == 'L':
        images_in_square = np.squeeze(images_in_square, 4)

    # Combine images to grid image
    new_im = Image.new(mode, (images.shape[1] * save_size, images.shape[2] * save_size))
    for col_i, col_images in enumerate(images_in_square):
        for image_i, image in enumerate(col_images):
            im = Image.fromarray(image, mode)
            new_im.paste(im, (col_i * images.shape[1], image_i * images.shape[2]))

    return new_im


# In[4]:


show_n_images = 1
mnist_images = get_batch(glob(os.path.join(resized_dataset_dir, '*.jpg'))[:show_n_images], 64, 64, 'RGB')
plt.imshow(images_square_grid(mnist_images, 'RGB'))


# ### Construção do Modelo

# In[5]:


learning_rate = 0.0001
epochs = 10
batch_size = 333
z_dimension = 100

display_step = 1

n_inputs = 16384
input_shape = [None, 128, 128, 3]


# In[6]:


X = tf.placeholder(tf.float32, input_shape, name="X")
Z = tf.placeholder(tf.float32, (None, z_dimension), name="Z")


# #### Rede Geradora

# In[7]:


def generator(z):

    #Fully connected layer
    fc1 = tf.layers.dense(z, 8*8*1024)
    #Reshape
    fc1 = tf.reshape(fc1,(-1, 8, 8, 1024))
    #Leaky relu
    fc1 = tf.nn.leaky_relu(fc1, alpha=0.2)

    # PRIMEIRA CAMADA DA CONVOLUCIONAL
    #Transpose Convolutional
    trans_conv1 = tf.layers.conv2d_transpose(inputs=fc1,
                                             filters=512,
                                             kernel_size=[5,5],
                                             strides=[2,2],
                                             padding='SAME',
                                             kernel_initializer=tf.truncated_normal_initializer(stddev=0.02))
    #Batch Normalization
    batch_trans_conv1 = tf.layers.batch_normalization(inputs=trans_conv1,
                                                      training=True,
                                                      epsilon=1e-5)
    #LeakyRelu
    relu_trans_conv1 = tf.nn.leaky_relu(batch_trans_conv1, alpha=0.2)


    # SEGUNDA CAMADA CONVOLUCIONAL
    #Transpose Convolutional
    trans_conv2 = tf.layers.conv2d_transpose(inputs=relu_trans_conv1,
                                             filters=256,
                                             kernel_size=[5,5],
                                             strides=[2,2],
                                             padding='SAME',
                                             kernel_initializer=tf.truncated_normal_initializer(stddev=0.02))
    #Batch Normalization
    batch_trans_conv2 = tf.layers.batch_normalization(inputs=trans_conv2,
                                                      training=True,
                                                      epsilon=1e-5)
    #LeakyRelu
    relu_trans_conv2 = tf.nn.leaky_relu(batch_trans_conv2, alpha=0.2)

    # TERCEIRA CAMADA CONVOLUCIONAL
    #Transpose Convolutional
    trans_conv3 = tf.layers.conv2d_transpose(inputs=relu_trans_conv2,
                                             filters=128,
                                             kernel_size=[5,5],
                                             strides=[2,2],
                                             padding='SAME',
                                             kernel_initializer=tf.truncated_normal_initializer(stddev=0.02))
    #Batch Normalization
    batch_trans_conv3 = tf.layers.batch_normalization(inputs=trans_conv3,
                                                      training=True,
                                                      epsilon=1e-5)
    #LeakyRelu
    relu_trans_conv3 = tf.nn.leaky_relu(batch_trans_conv3, alpha=0.2)

    # QUARTA CAMADA CONVOLUCIONAL
    #Transpose Convolutional
    trans_conv4 = tf.layers.conv2d_transpose(inputs=relu_trans_conv3,
                                             filters=512,
                                             kernel_size=[5,5],
                                             strides=[2,2],
                                             padding='SAME',
                                             kernel_initializer=tf.truncated_normal_initializer(stddev=0.02))
    #Batch Normalization
    batch_trans_conv4 = tf.layers.batch_normalization(inputs=trans_conv4,
                                                      training=True,
                                                      epsilon=1e-5)
    #LeakyRelu
    relu_trans_conv4 = tf.nn.leaky_relu(batch_trans_conv4, alpha=0.2)


    #ULTIMA CONVOLUÇÂO E TANH
    logits = tf.layers.conv2d_transpose(inputs=relu_trans_conv4,
                                        filters=3,
                                        kernel_size=[5,5],
                                        strides=[1,1],
                                        padding='SAME')

    result = tf.tanh(logits)
    
    return result


# In[8]:


def discriminator(x):
    
    #PRIMEIRA CAMADA CONVOLUCIONAL
    conv1 = tf.layers.conv2d(inputs = x,
                        filters = 64,
                        kernel_size = [5,5],
                        strides = [2,2],
                        padding = "SAME",
                        kernel_initializer=tf.truncated_normal_initializer(stddev=0.02))
    batch_norm1 = tf.layers.batch_normalization(conv1,
                                               training = True,
                                               epsilon = 1e-5)
    relu1 = tf.nn.leaky_relu(batch_norm1, alpha=0.2)
    
    #SEGUNDA CAMADA CONVOLUCIONAL
    conv2 = tf.layers.conv2d(inputs = relu1,
                        filters = 128,
                        kernel_size = [5,5],
                        strides = [2,2],
                        padding = "SAME",
                        kernel_initializer=tf.truncated_normal_initializer(stddev=0.02))
    batch_norm2 = tf.layers.batch_normalization(conv2,
                                               training = True,
                                               epsilon = 1e-5)
    relu2 = tf.nn.leaky_relu(batch_norm2, alpha=0.2)
    
    #TERCEIRA CAMADA CONVOLUCIONAL
    conv3 = tf.layers.conv2d(inputs = relu2,
                        filters = 256,
                        kernel_size = [5,5],
                        strides = [2,2],
                        padding = "SAME",
                        kernel_initializer=tf.truncated_normal_initializer(stddev=0.02))
    batch_norm3 = tf.layers.batch_normalization(conv3,
                                               training = True,
                                               epsilon = 1e-5)
    relu3 = tf.nn.leaky_relu(batch_norm3, alpha=0.2)
    
    #QUARTA CAMADA CONVOLUCIONAL
    conv4 = tf.layers.conv2d(inputs = relu3,
                        filters = 512,
                        kernel_size = [5,5],
                        strides = [1,1],
                        padding = "SAME",
                        kernel_initializer=tf.truncated_normal_initializer(stddev=0.02))
    batch_norm4 = tf.layers.batch_normalization(conv4,
                                               training = True,
                                               epsilon = 1e-5)
    relu4 = tf.nn.leaky_relu(batch_norm4, alpha=0.2)
    
    #QUINTA CAMADA CONVOLUCIONAL
    conv5 = tf.layers.conv2d(inputs = relu4,
                        filters = 1024,
                        kernel_size = [5,5],
                        strides = [2,2],
                        padding = "SAME",
                        kernel_initializer=tf.truncated_normal_initializer(stddev=0.02))
    batch_norm5 = tf.layers.batch_normalization(conv5,
                                               training = True,
                                               epsilon = 1e-5)
    relu5 = tf.nn.leaky_relu(batch_norm5, alpha=0.2)
    
    
    #FLATTEN e FULLY CONNECTED
    flatten = tf.reshape(relu5, (-1, 8*8*1024))
    logits = tf.layers.dense(inputs = flatten,
                                units = 1,
                                activation = None)
    result = tf.sigmoid(logits)
    
    return result, logits


# #### Resultado, Loss E Optimizers

# In[9]:


generator_result = generator(Z)
discriminator_result_true, discriminator_logits_true = discriminator(X)
discriminator_result_fake, discriminator_logits_fake = discriminator(generator_result)


# In[10]:


loss_discriminator_true = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=discriminator_logits_true, labels= tf.ones_like(discriminator_logits_true)))
loss_discriminator_fake = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=discriminator_logits_fake, labels= tf.zeros_like(discriminator_logits_fake)))
loss_discriminator = loss_discriminator_true + loss_discriminator_fake

loss_generator = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=discriminator_logits_fake, labels= tf.ones_like(discriminator_logits_fake)))


# In[11]:


optimizer_discriminator = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss_discriminator)

optimizer_generator = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss_generator)


# #### Rodando Treino e Teste

# In[12]:






def images_square_grid(images, mode):
    """
    Save images as a square grid
    :param images: Images to be used for the grid
    :param mode: The mode to use for images
    :return: Image of images in a square grid
    """
    # Get maximum size for square grid of images
    save_size = math.floor(np.sqrt(images.shape[0]))

    # Scale to 0-255
    images = (((images - images.min()) * 255) / (images.max() - images.min())).astype(np.uint8)

    # Put images in a square arrangement
    images_in_square = np.reshape(
            images[:save_size*save_size],
            (save_size, save_size, images.shape[1], images.shape[2], images.shape[3]))
    if mode == 'L':
        images_in_square = np.squeeze(images_in_square, 4)

    # Combine images to grid image
    new_im = Image.new(mode, (images.shape[1] * save_size, images.shape[2] * save_size))
    for col_i, col_images in enumerate(images_in_square):
        for image_i, image in enumerate(col_images):
            im = Image.fromarray(image, mode)
            new_im.paste(im, (col_i * images.shape[1], image_i * images.shape[2]))
    return new_im


# def sample_Z(m, n):
#     return np.random.uniform(-1., 1., size=[m, n])


# In[13]:


a = np.random.uniform(-1., 1., size=[12, z_dimension])
print(a.shape)


# In[ ]:


with tf.Session() as sess:
    start_time = time.time()
    sess.run(tf.global_variables_initializer())
    for i in range(epochs):
        
        if i % display_step == 0:
            z = np.random.uniform(-1., 1., size=[12, z_dimension])
            result = sess.run(generator_result, feed_dict={Z:z})
            fig = images_square_grid(result, 'RGB')
            fig.save('dogs_gan_outputs/{}.png'.format(str(i).zfill(3)))
#             plt.savefig('dogs_gan_outputs/{}.png'.format(str(i).zfill(3)), bbox_inches='tight')
#             plt.close(fig)
        
        x = get_batch(glob(os.path.join(resized_dataset_dir, '*.jpg')), 128, 128, 'RGB')
        
        _, discriminator_loss_curr = sess.run([optimizer_discriminator, loss_discriminator], feed_dict={X:x, Z:z})
        _, generator_loss_curr = sess.run([optimizer_generator, loss_generator], feed_dict={X:x, Z:z})
        
        end_time_epoch = time.time()
        
        if i % display_step == 0:
            print("\nXXXXXXXXXXXX")
            print("Epoch :", i )
            print("Discriminator loss :", discriminator_loss_curr)
            print("Generator loss :", generator_loss_curr)
            print("Time to run epoch:", end_time_epoch - start)
    
    end_time = time.time()
    print("Execution time :", end-start)
    print("Numero de epocas", epochs)
    

